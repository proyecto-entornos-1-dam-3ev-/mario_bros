package Principal;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

enum formas {
	Quieto,
	Salto_Iz,
	Salto_Der,
	Izquierda1,
	Izquierda2,
	Izquierda3,
	Derecha1,
	Derecha2,
	Derecha3
}

public class Main extends JPanel {

	// JFrame
	private static JFrame marco;
	
	// Dimensiones de la ventana (JFrame)
    public final static int ANCHO = 1200;
    public final static int ALTO = 600;
    
    // Bloques
    public static int ANCHO_bloques = 48;
    public static int ALTO_bloques = 50;
    
    // Fondo
    public float ANCHO_fondo = ANCHO;
    public int ALTO_fondo = ALTO + 120;
	
		// Posicion de la foto del fondo
    public float x_fondo = 0;
	
		// Final del fondo
    public int x_final = 9100;
    
	// Posicion del juego en todo momento
    public float xGame = 0;
    public float yGame = 0;
	
	// Suelo donde se situara Mario
    public static int suelo = ALTO_bloques * 9 + 20;

	// Velocidad de Mario
    public double velocidad = .17;
    
	// Mario
    Mario mario = new Mario(0, 0, suelo, 0);
    
		// Posicion de Mario
    public float x = 0, y = suelo;
    public static int suelo_Mario = ALTO_bloques * 9 + 20; 
    
    	// Personaje (Mario)
    public static int ANCHO_Mario = ANCHO_bloques;
    public int ALTO_Mario = ALTO_bloques;
        
    	// Longitud del salto de Mario
    public double longitudSalto = ALTO_bloques * 3.5;
    	// Forma del sprite de Mario
    public int forma;
    
    // Posicion del fuego
    public double fuegoPosXini;
    public double fuegoPosX;
    public double fuegoPosXReal;
    public double fuegoPosY;
    public boolean fire;
    
	// Para la animacion de moverse
	private int animacion = 0;
	private int animacion1 = 0;
    
    // Botones (booleand para saber si se presionan o no)
    public boolean arriba, salto, abajo, caida, izquierda, derecha, mover_arriba_menu, mover_abajo_menu;
    public boolean musicaOn, SonidosOn;
	
	// Menu
    public boolean entrar, salir, salir_fin;
    public int menu;
    
    // Fin de la partida
    public boolean end = false;
    public boolean empieza = false;
    
    //Vidas
    public int vidas;
    
    // Coins
    public int coin;
    // Coje Moneda
    public boolean cojeMonedaBool;
    public int cojeMonedaInt;
    
	// Imagenes
    public Image fondo;
    public Image game_over;
    public Image logo;
    public Image flecha;
    public Image mario_fin;
    public Image mario_fin2;
    
	public Image caja;
	public Image obstaculo;
	
	public Image mario_sprite;
    public Image vida_Mario;
	
	public Image seta;
	public Image flor;
    public Image fuego;
    public Image moneda;
    
    public static Main comienzo;
    
    Music musicafondo = new Music("src/Music/mariotrvp.wav");
    Music musicaSalto = new Music("src/Music/salto.wav");
    Music musicaFuego = new Music("src/Music/bolaFuego.wav");
    Music musicaFinal = new Music("src/Music/world_clear.wav");
    Music musicaMuerto = new Music("src/Music/marioDie.wav");
	
	// Posiciones de las setas
	public int[][] posSeta;

	// Posiciones de las flores
	public int[][] posFlor;
	
	// Posiciones de los obstaculos
	public int[][] posObstaculos = new int[][] {
		{20, 0},
		{21, 0}, {21, 1},
		{22, 0},
		{31, 0},
		{50,0},
		{51,0}, {51,1},
		{52,0},
		{60,0},
		{70,0},
		{71,0}, {71,1},
		{72,0}, {72,1}, {72,2},
		{73,0}, {73,1}, {73,2}, {73,3},
		{78,0}, {78,1}, {78,2}, {78,3},
		{79,0}, {79,1}, {79,2},
		{80,0}, {80,1},
		{81,0},
		{87,0},
		{89,0}, {89,1},
		{91,0},
		{93,0}, {93,1}, {93,2},
		{105,0},
		{106,0},{106,1},
		{107,0},{107,1},{107,2},
		{108,0},{108,1},
		{109,0},
		{123,0},
		{124,0}, {124,1},
		{125,0}, {125,1}, {125,2},
		{126,0}, {126,1}, {126,2}, {126,3},
		{127,0}, {127,1}, {127,2},
		{128,0}, {128,1},
		{129,0},
		{130,0},
		{131,0},
		{132,0},
		{133,0},
		{134,0},
		{135,0},
		{136,0}, {136,1},
		{137,0}, {137,1}, {137,2},
		{138,0}, {138,1}, {138,2}, {138,3},
		{139,0}, {139,1}, {139,2}, 
		{140,0}, {140,1},
		{141,0},
		{159,0},
		{161,0}, {161,1},
		{163,0}, {163,1}, {163,2},
		{165,0}, {165,1}, {165,2}, {165,3},
		{166,0}, {166,1}, {166,2}, {166,3}, {166,4},
		{167,0}, {167,1}, {167,2}, {167,3},
		{168,0}, {168,1}, {168,2}, {168,3}, {168,4},
		{169,0}, {169,1}, {169,2}, {169,3},
		{170,0}, {170,1}, {170,2}, {170,3}, {170,4},
		{171,0}, {171,1}, {171,2}, {171,3},
		{185,0},
		{186,0}, {186,1},
		{187,0}, {187,1}, {187,2},
		{188,0}, {188,1}, {188,2}, {188,3},
		{189,0}, {189,1}, {189,2}, {189,3}, {189,4},
		{190,0}, {190,1}, {190,2}, {190,3}, {190,4}, {190,5},
		{191,0}, {191,1}, {191,2}, {191,3}, {191,4}, {191,5}, {191,6}
	};
	
	// Posiciones de las cajas
	Cajas[] cajasSorpresa = {
			new Cajas("cajaSorpresa", 6, 3),
			new Cajas("cajaSorpresa", 9, 3),
			new Cajas("cajaSorpresa", 28,3),
			new Cajas("cajaSorpresa", 35,3),
			new Cajas("cajaSorpresa", 46,3),
			new Cajas("cajaSorpresa", 56,3),
			new Cajas("cajaSorpresa", 66,3),
			new Cajas("cajaSorpresa", 65,3),
			new Cajas("cajaSorpresa", 78,7),
			new Cajas("cajaSorpresa", 98,3),
			new Cajas("cajaSorpresa", 131,4),
			new Cajas("cajaSorpresa", 132,4),
			new Cajas("cajaSorpresa", 133,4),
			new Cajas("cajaSorpresa", 146,3),
			new Cajas("cajaSorpresa", 147,3),
			new Cajas("cajaSorpresa", 148,3),
			new Cajas("cajaSorpresa", 149,3)
	};
	
	// Clase de los enemigos
	Enemigos[] enemigos = {
			new Enemigos("goomba", 15, 0,  0, 19, 1),
			new Enemigos("goomba", 11, 0,  0, 19, 1),
			new Enemigos("goomba", 25, 0, 23, 30, 1),
			new Enemigos("goomba", 32, 0, 32, 49, 1),
			new Enemigos("goomba", 53, 0, 53, 59, 1),
			new Enemigos("goomba", 61, 0, 61, 69, 1),
			new Enemigos("goomba", 74, 0, 74, 77, 1),
			new Enemigos("goomba", 75, 0, 74, 77, 1),
			new Enemigos("goomba", 76, 0, 74, 77, 1),
			new Enemigos("goomba", 74, 0, 74, 77, -1),
			new Enemigos("goomba", 75, 0, 74, 77, -1),
			new Enemigos("goomba", 76, 0, 74, 77, -1),
			new Enemigos("goomba", 82, 0, 82, 86, 1),
			new Enemigos("goomba", 94, 0, 94, 104, 1),
			new Enemigos("goomba", 110, 0, 110, 122, 1),
			new Enemigos("goomba", 112, 0, 110, 122, 1),
			new Enemigos("goomba", 115, 0, 110, 122, 1),
			new Enemigos("goomba", 130, 1, 129, 135, 1),
			new Enemigos("goomba", 142, 0, 142, 158, 1),
			new Enemigos("goomba", 148, 0, 142, 158, 1),
			new Enemigos("goomba", 155, 0, 142, 158, 1),
			new Enemigos("goomba", 174, 0, 172, 184, 1)		
	};

	// Flecha del menu
	private int[][] posFlecha = new int[][] {
		{400, ALTO/2 - 35 + 50},
		{450, ALTO/2 - 35 + 100},
		{450, ALTO/2 - 35 + 150}
	};
	private double flecha_num = 999;
	
	public Main() {
    	menu = 1;
        x = 0;
        y = suelo_Mario;
        musicaOn = true;
        SonidosOn = true;
        vidas = 3;
        coin = 0;
        
        iniciar_Bonificaciones ();
                
    	fondo = new ImageIcon("src/Media/fondo-Mario.png").getImage();
    	game_over = new ImageIcon("src/Media/game_over.png").getImage();
    	logo = new ImageIcon("src/Media/mario_logo.png").getImage();
    	flecha = new ImageIcon("src/Media/flecha.png").getImage();
    	mario_fin = new ImageIcon("src/Media/mario2.gif").getImage();
    	mario_fin2 = new ImageIcon("src/Media/mario3.gif").getImage();
    	
    	caja = new ImageIcon("src/Media/cajaSorpresa.png").getImage();
    	obstaculo = new ImageIcon("src/Media/obstaculo.png").getImage();
    	
    	mario_sprite = new ImageIcon("src/Media/sprite_Mario.png").getImage();
    	vida_Mario = new ImageIcon("src/Media/mario_vida.PNG").getImage();
    	
    	seta = new ImageIcon("src/Media/bonificaciones/seta.png").getImage();
    	flor = new ImageIcon("src/Media/bonificaciones/flor_fuego.png").getImage();
    	moneda = new ImageIcon("src/Media/bonificaciones/moneda.png").getImage();
    	fuego = new ImageIcon("src/Media/bonificaciones/fireball.png").getImage();
    	    	
    	// Crea unas dimensiones de donde se va a actuar
    	// El JFrame es todo pero se pone todo en el JPanel
        setPreferredSize(new Dimension(ANCHO, ALTO));
        setFocusable(true);

    	addKeyListener(new KeyAdapter() {
        		// Hay que poner como minimo estos dos eventos
        		// Pressed actua cuando se presiona 
        		// Released actua cuando se deja de presionar
                public void keyPressed(KeyEvent e) {
                    actualiza(e.getKeyCode(), true);
                    actualiza_musica(e.getKeyCode());
                }
                
                public void keyReleased(KeyEvent e) {
                    actualiza(e.getKeyCode(), false);
                }

                private void actualiza(int keyCode, boolean pressed) {
                	// Botones menu
                	if(!empieza) {
                		switch (keyCode) {
                        	case KeyEvent.VK_UP:
                        	case KeyEvent.VK_W:
                        		flecha_num -= 0.5;
                        		break;
                        		
                        	case KeyEvent.VK_DOWN:
                        	case KeyEvent.VK_S:
                        		flecha_num += 0.5;
                            	break;
                            	
                        	case KeyEvent.VK_ENTER:
                        		entrar = true;
                        		break;
                        		
                        	case KeyEvent.VK_ESCAPE:
                        		salir = true;
                        		break;
                    	}
                	}
                	// Botones juego
                	else {
                		switch (keyCode) {
                		
                    		case KeyEvent.VK_UP:
                    		case KeyEvent.VK_W:
                    			if (!caida)
                    				salto = true;
                    			break;
                    			
                    		case KeyEvent.VK_LEFT:
                    		case KeyEvent.VK_A:
                    			izquierda = pressed;
                    			break;
                    			
                    		case KeyEvent.VK_RIGHT:
                    		case KeyEvent.VK_D:
                    			derecha = pressed;
                    			break;

                    		case KeyEvent.VK_SPACE:
                    			if (mario.getTamanio() == 2 && !fire)
                    				lanza_fuego();
                    			break;
                		}
                	}
                }
                
                private void actualiza_musica(int keyCode) {
                    switch (keyCode) {
                    case KeyEvent.VK_O:
                    	if (!musicaOn) {
                           	musicafondo.play();
                           	musicaOn = true;
                        }
                    	else {
                    		musicafondo.stop();
                    		musicaOn = false;
                       	}
                    break;
                    case KeyEvent.VK_I:
                    	if (!SonidosOn)
                    		SonidosOn = true;
                    		
                    	else
                    		SonidosOn = false;
                    	
                    	break;
                   
                    case KeyEvent.VK_SPACE:
                    	if (SonidosOn && mario.getTamanio() == 2)
                    		musicaFuego.play();
                    	break;
                    case KeyEvent.VK_ENTER:
                		salir_fin = true;
                		break;
                		
                    case KeyEvent.VK_UP:
                    case KeyEvent.VK_W:
                    	if (salto && SonidosOn && !caida) {
                           	musicaSalto.play();
                        }
                    break;
                    }
                }
            });
    }

	public static void main (String[] args) throws Exception {
    	
    	marco = new JFrame("Mario Bros");
        // NO hace falta pero esta bien ponerlo
    	marco.addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent e) {
    			System.exit(0);
    		}
    	});
    	marco.setResizable(false);
    	marco.setEnabled(true);
    	marco.setSize(new Dimension(ANCHO, ALTO));
    	marco.setLocationRelativeTo(null);
    	comienzo = new Main();
        marco.getContentPane().add(comienzo);
        marco.pack();
        marco.setVisible(true);
        comienzo.cicloJuego();        
    }
    
	public void paint(Graphics g) {
    	super.paint(g);
    	
    	if (!empieza) {

        	// Fondo
    		g.setColor(Color.black);
    		g.fillRect(0, 0, ANCHO, ALTO);
    		
    		// Titulo
    		g.drawImage(logo, 300, 50, 600, 200, null);
    		
    		// Musica
    		g.setFont(new Font("TimesRoman", Font.BOLD, 25));
    		g.setColor(Color.RED);
    		g.drawString("Musica de Fondo On/Off --> O", 20, 40);

    		switch (menu) {
    			case 1:
    				menuP (g);
    				break;
    			case 2:
    				menuControles (g);
    				break;
    			case 3:
    				menuCreditos (g);
    				break;
    			case 4:
    				fin (g);
    				break;
    		}
    	}
    	else if (!end)
    		juego (g);
    	else if (end && vidas < 0)
    		fin (g);
    	else if (end && vidas >= 0)
    		menuWin (g);
    }

    public void menuP (Graphics g) {
		
		// Flecha
		g.drawImage(flecha, posFlecha[(int) flecha_num % posFlecha.length][0], posFlecha[(int) flecha_num % posFlecha.length][1], 70, 50, null);
		
		//Imagen de las vidas
		g.setFont(new Font("TimesRoman", Font.BOLD, 35));
		g.setColor(Color.RED);
		g.drawString("   Vidas  " + vidas, 100, 550);
		g.drawImage(vida_Mario, 10, 480, ANCHO_bloques * 2, ALTO_bloques * 2, null);
		
		// Menus
		g.setColor(Color.RED);
		// Empezaro
		g.setFont(new Font("TimesRoman", Font.BOLD, 30));
		g.drawString("Empezar a JUGAR", 495, ALTO/2 + 50);
		// CreditosO
		g.setColor(Color.CYAN);
		g.drawString("Controles", 550, ALTO/2 + 100);
		// Creditos
		g.setColor(Color.GREEN);
		g.drawString("Creditos", 560, ALTO/2 + 150);
	}
    
    public void menuCreditos (Graphics g) {
    	
		// Creditos
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		
		g.drawString("Jorge Loarte", 400, 300);
		g.drawString("Alejandro Gutierrez", 400, 350);
		g.drawString("Marta Gomez", 400, 400);
		g.drawString("Alejandro Cartes", 400, 450);
		g.drawString(" Especial agradecimientos a Kacper Duda y �lvaro Sanchez", 80, 550);
	}
    
    public void menuControles (Graphics g) {
		
		// Controles
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		g.drawString("Salto --> UP, W", 300, 330);
		g.drawString("Movimiento derecha --> RIGHT, D", 300, 380);
		g.drawString("Movimiento izquierda --> LEFT, A", 300, 430);
		g.drawString("Lanza Fuego --> SPACE", 300, 480);
		g.drawString("Cambiar Sonidos On/Off --> I", 300, 530);
	}

    public void juego (Graphics g) {
    	
		// Fondo
		g.drawImage(fondo, 0, 0, ANCHO, ALTO, Math.round(x_fondo), 0, Math.round(ANCHO_fondo), Math.round(ALTO_fondo), null);
		
		// Mario
		g.drawImage(mario_sprite, (int) x, (int) y, (int) x + ANCHO_Mario, (int) y + mario.getAltoMario(),
				mario.forma_Mario[mario.getTamanio()][forma][0],
				mario.forma_Mario[mario.getTamanio()][forma][1],
				mario.forma_Mario[mario.getTamanio()][forma][2],
				mario.forma_Mario[mario.getTamanio()][forma][3],
				null);
		
		// Bola de fuego
		if (fire)
			g.drawImage(fuego, (int) fuegoPosX, (int) fuegoPosY, ANCHO_bloques / 2, ALTO_bloques / 2, null);
		
		// Monedas
		g.setFont(new Font("Comics Sans", Font.BOLD, 25));
		g.setColor(Color.BLACK);
		g.drawString("Coins: " + coin, 50, 50);
		
		// Dibujar cajas sorpresa
		for (int i = 0; i < cajasSorpresa.length; i++)
			dibujaBloques(g, cajasSorpresa[i].getImagen(), ANCHO_bloques * cajasSorpresa[i].getPosX(), suelo - ALTO_bloques * cajasSorpresa[i].getPosY(), ANCHO_bloques,ALTO_bloques);

		// Dibujar obstaculos
		for (int i = 0; i < posObstaculos.length; i++)
			dibujaBloques(g, obstaculo, ANCHO_bloques * posObstaculos[i][0], suelo - ALTO_bloques * posObstaculos[i][1], ANCHO_bloques,ALTO_bloques);
		
		// Dibujar Setas
		for (int i = 0; i < posSeta.length; i++)
			dibujaBloques(g, seta, ANCHO_bloques * posSeta[i][0], suelo + 4, ANCHO_bloques, ALTO_bloques);

		// Dibujar Flores
		for (int i = 0; i < posFlor.length; i++)
		dibujaBloques(g, flor, ANCHO_bloques * posFlor[i][0], suelo, ANCHO_bloques, ALTO_bloques);
		
		// Dibujar Enemigos
		for (int i = 0; i < enemigos.length; i++)
			dibujaBloques(g, enemigos[i].getImagen(),
					(int) enemigos[i].getPosX(), (int) enemigos[i].getPosY(),
					ANCHO_bloques, ALTO_bloques);
	}
    
    public void fin (Graphics g) {

    	// Game Over
		g.drawImage(game_over, 0, 0, ANCHO, ALTO, null);
	}
    
    public void menuWin (Graphics g) {
    	// Fondo
		g.setColor(Color.black);
		g.fillRect(0, 0, ANCHO, ALTO);
		
		// Logo
		g.drawImage(logo, 100, 50, 400, 200, null);
		g.drawImage(mario_fin2, 550, 100, 750, 500, null);

		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		g.drawString("HAS COMPLETADO EL NIVEL", 70, 400);
    }
    
    public void dibujaBloques(Graphics g, Image img, int xObject, int yObject, int width, int height) {
    	float xMin = (xGame - x);
    	float xMax = xMin + ANCHO;
    	if ( xObject + width >= xMin && xObject <= xMax )
    		g.drawImage(img, (int) (xObject - xGame + x), (int) (yObject - yGame), width, height, null);
    }

    // Funcion para repintar lo que hay en el paint
    private void dibuja() throws Exception {
		SwingUtilities.invokeAndWait(new Runnable() {
			public void run() {
				repaint();
			}
		});
    } 
    
    // Funcion del ciclo    
    public void cicloJuego() throws Exception {
    	if (musicaOn)
    		musicafondo.play();
    	while (!empieza) {
            dibuja();
            if (entrar) {
            	if (flecha_num % posFlecha.length == 0)
            		// Empieza el juego
            		empieza = true;
            	else if (flecha_num % posFlecha.length == 1) {
            		// Controles
            		menu = 2;
            		entrar = false;
            	}
            	else {
            		// Creditos
            		menu = 3;
        			entrar = false;
        		}
            }
            if (salir && menu != 1) {
            	// Menu Principal
            	menu = 1;
            	salir = false;
            }
    	}
    	musicaMuerto.stop();
    	while (!end) {
    		movimientos();
    		movimientos_Enemigos();
    		if (choque_enemigo()) {
    			end = true;
    		}
    		if (fire)
    			movimiento_fuego();
    		else
    			musicaFuego.stop();
        	formas();
        	cojeSeta();
        	cojeFlor();
           	dibuja();
           	if (x_fondo >= x_final)
           		end = true;
       	}
        musicafondo.stop();
    	if (x_fondo >= x_final) {
    		animacionFinal();
    	}
    	else
    		finalPrograma();
    }

	// Funcion con los movimientos
    private void movimientos() {
    	// Mov. ir a la Izquierda
        if (izquierda) {
        	if (!choque()) {
        		if (xGame > 0)
        			xGame -= velocidad;
        		if (xGame > 400) {
        			x_fondo -= velocidad;
        			ANCHO_fondo -= velocidad;
        		}
        		else if (x > 0)
        			x -= velocidad;
        	}
        	else
        		salto = false;
        }
        
        // Mov. ir a la derecha
        if (derecha) {
        	if ( !choque() ) {
        		xGame += velocidad;
            	if (xGame > 400) {
            		x_fondo += velocidad;
            		ANCHO_fondo += velocidad;
            	}
            	else
            		x += velocidad;
        	}
        	else
        		salto = false;
        }
        
        // Mov. salto
        if (salto && !choque()) {
        	y -= velocidad;
        	caida = false;
        	if (y < suelo_Mario - mario.getLongitudSalto()) {
        		salto = false;
        		caida = true;
        	}
        }
        
        // Mov. caida
        if (caida)
        	caidaFun();
        else if (!salto) {
        	musicaSalto.stop();
			if (mario.getTamanio() == 0)
				mario.setLongitudSalto(mario.getAltoMario() * 3);
			else
				mario.setLongitudSalto(mario.getAltoMario() / 2 * 3);
        	verSuelo();
        }
    }

    // Movimiento de los enemigos
    public void movimientos_Enemigos () {
    	for (int i = 0; i < enemigos.length; i++) {
    		if (enemigos[i].getPosX() <= enemigos[i].getPosParadaIz())
    			enemigos[i].setDir(1);
    		else if(enemigos[i].getPosX() >= enemigos[i].getPosParadaDer())
    			enemigos[i].setDir(-1);
    	
    		enemigos[i].setPosX(enemigos[i].getPosX() + enemigos[i].getDir() * velocidad/2);
    	}
    }
    
    public void movimiento_fuego () {
    	// Choque de goomba y el fuego
    	for (int i = 0; i < enemigos.length; i++) {
    		if ( Math.round(fuegoPosXReal) == Math.round(enemigos[i].getPosX()) &&
    				Math.round(fuegoPosY) == Math.round(enemigos[i].getPosY()) ) {
 				enemigos[i].setPosY(-100);
    			fire = false;
    			break;
    		}
    	}
    	for (int i = 0; i < posObstaculos.length && fire; i++) {
    		if ( Math.round(fuegoPosXReal / ANCHO_bloques) == Math.round(posObstaculos[i][0]) &&
    				Math.round(fuegoPosY) == Math.round(suelo - posObstaculos[i][1] * ALTO_bloques) ) {
    			fire = false;
    			break;
    		}
    	}
    	if (fire && fuegoPosX < fuegoPosXini + ANCHO_bloques * 7) {
    		fuegoPosX += velocidad * 2;
    		fuegoPosXReal += velocidad * 2;
    	}
    	else
    		fire = false;
    }
    
    // Funcion de choque de mario
    public boolean choque() {
    	
    	boolean hasChocado = false;
    	// Obstaculos
		for (int i = 0; i < posObstaculos.length; i++) {
			if (mario.getTamanio() == 0) {
				if ( derecha &&
						Math.floor(xGame / ANCHO_bloques) + 1 == posObstaculos[i][0] &&
						suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] )
    			{
    				hasChocado = true;
    			}
    			if ( izquierda &&
    					Math.ceil(xGame / ANCHO_bloques) - 1 == posObstaculos[i][0] &&
    					suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] )
    			{
    				hasChocado = true;
    			}
    			if ( salto &&
    					Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
    					suelo / ALTO_bloques - suelo_Mario / mario.getAltoMario() - mario.getLongitudSalto() / mario.getAltoMario() >= posObstaculos[i][1] )
    			{
    				mario.setLongitudSalto(mario.getLongitudSalto() - mario.getAltoMario());
    				hasChocado = true;
    			}
    		}
	    	else {
	        	if ( derecha &&
	        			Math.floor(xGame / ANCHO_bloques) + 1 == posObstaculos[i][0] &&
	        			(suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] ||
	        			 suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) - 1 == posObstaculos[i][1] )
	        			)
	        	{
	        		hasChocado = true;
	        	}
	        	if ( izquierda &&
	        			Math.ceil(xGame / ANCHO_bloques) - 1 == posObstaculos[i][0] &&
	        			(suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] ||
	           			suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) - 1 == posObstaculos[i][1] )
	              		)
	            {
	        		hasChocado = true;
	        	}
        		if ( Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
        				9 - suelo_Mario / ALTO_bloques - 1 <= posObstaculos[i][1])
        		{
        				mario.setLongitudSalto(ALTO_bloques);
        				hasChocado = true;
        		}
	    	}
		}
    	// Cajas Sorpresa
		for (int i = 0; i < cajasSorpresa.length; i++) {
			if (mario.getTamanio() == 0) {
				if ( derecha &&
    					Math.floor(xGame / ANCHO_bloques) + 1 == cajasSorpresa[i].getPosX() &&
    					suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == cajasSorpresa[i].getPosY() )
    			{
    				hasChocado = true;
    			}
    			if ( izquierda &&
    					Math.ceil(xGame / ANCHO_bloques) - 1 == cajasSorpresa[i].getPosX() &&
    					suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == cajasSorpresa[i].getPosY() )
    			{
    				hasChocado = true;
    			}
    			if ( salto &&
    					Math.round(xGame / ANCHO_bloques) == cajasSorpresa[i].getPosX() &&
    					suelo / ALTO_bloques - suelo_Mario / mario.getAltoMario() + mario.getLongitudSalto() / mario.getAltoMario() >= cajasSorpresa[i].getPosY() )
    			{
    				mario.setLongitudSalto(mario.getLongitudSalto() - mario.getAltoMario());
    				cojeMonedaBool = true;
    				cojeMonedaInt = i;
    				hasChocado = true;	
    			}
			}
			else {
        		if ( derecha &&
        				Math.floor(xGame / ANCHO_bloques) + 1 == cajasSorpresa[i].getPosX() &&
        				(suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == cajasSorpresa[i].getPosY() ||
        				 suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) - 1 == cajasSorpresa[i].getPosY() )
        				)
        		{
        			hasChocado = true;
        		}
        		if ( izquierda &&
        				Math.ceil(xGame / ANCHO_bloques) - 1 == cajasSorpresa[i].getPosX() &&
        				(suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == cajasSorpresa[i].getPosY() ||
               			suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) - 1 == cajasSorpresa[i].getPosY() )
               			)
               	{
        			hasChocado = true;
        		}
        		if ( salto &&
        				Math.round(xGame / ANCHO_bloques) == cajasSorpresa[i].getPosX() &&
        				suelo / ALTO_bloques - suelo_Mario / ALTO_bloques - 1 < cajasSorpresa[i].getPosY())
        		{
        				mario.setLongitudSalto(ALTO_bloques);
        				cojeMonedaBool = true;
        				cojeMonedaInt = i;
        		}
        	}
    	}
    	return hasChocado;
    }
    
    boolean choque_enemigo () {
    	for (int i = 0; i < enemigos.length; i++) {
    		if (mario.getTamanio() == 0) {
    			if ( Math.round(xGame / ANCHO_bloques) == Math.round(enemigos[i].getPosX() / ANCHO_bloques) && !salto) {
    				// Este if es por si choca con Mario por arriba
    				if ( Math.floor(y / ALTO_bloques) + 1 == Math.round(enemigos[i].getPosY() / ALTO_bloques) ) {
    					enemigos[i].setPosY(-100);
    					return false;
    				}
    				// Este if es por si se choca con Mario por los lados
    				else if ( Math.round(y / ALTO_bloques) == Math.round(enemigos[i].getPosY() / ALTO_bloques) ) {
						enemigos[i].setPosY(-100);
    					return true;
    				}
    			}
    		}
    		else {
    			if ( Math.round(xGame / ANCHO_bloques) == Math.round(enemigos[i].getPosX() / ANCHO_bloques) && !salto) {
    				// Este if es por si choca con Mario por arriba
    				if ( Math.floor(y / ALTO_bloques) + 2 == Math.round(enemigos[i].getPosY() / ALTO_bloques) ) {
    					enemigos[i].setPosY(-100);
    					return false;
    				}
    				// Este if es por si se choca con Mario por los lados
    				else if ( Math.round(y / ALTO_bloques) + 1 == Math.round(enemigos[i].getPosY() / ALTO_bloques) ) {
    					if (mario.getTamanio() == 1)
    						cambiaTamanio("Peque");
    					else
    						cambiaTamanio("Grande");
						enemigos[i].setPosY(-100);
    					return false;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    // Funciones de la caida
    public void caidaFun() {
    	
    	for (int i = 0; i < posObstaculos.length && caida; i++) {
    		if (mario.getTamanio() == 0) {
    			if (Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
        				suelo / ALTO_bloques - suelo_Mario / mario.getAltoMario() == posObstaculos[i][1] )
        		{
        			suelo_Mario = (9 - posObstaculos[i][1] - 1) * ALTO_bloques + 20;
        		}
    		}
    		else {
    			if (Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
    					suelo / ALTO_bloques - suelo_Mario / (mario.getAltoMario() / 2) - 1 == posObstaculos[i][1] )
        		{
        			suelo_Mario = (9 - posObstaculos[i][1] - 2) * ALTO_bloques + 20;
        		}    			
    		}
    	}
    	
    	for (int i = 0; i < cajasSorpresa.length && caida; i++) {
    		if (mario.getTamanio() == 0) {
    			if (Math.round(xGame / ANCHO_bloques) == cajasSorpresa[i].getPosX() &&
        				suelo / ALTO_bloques - suelo_Mario / mario.getAltoMario() == cajasSorpresa[i].getPosY() )
        		{
        			suelo_Mario = (9 - cajasSorpresa[i].getPosY() - 1) * ALTO_bloques + 20;
        		}
    		}
    		else {
    			if (Math.round(xGame / ANCHO_bloques) == cajasSorpresa[i].getPosX() &&
    					suelo / ALTO_bloques - suelo_Mario / (mario.getAltoMario() / 2) - 1 == cajasSorpresa[i].getPosY() )
        		{
        			suelo_Mario = (9 - cajasSorpresa[i].getPosY() - 2) * ALTO_bloques + 20;
        		}    			
    		}
    	}
    	
    	if (cojeMonedaBool) {
			cojeMonedaBool = false;
			cojeMoneda(cojeMonedaInt);
		}
    		
    	if (y < suelo_Mario)
    		y += velocidad;
    	else
    		caida = false;
    }
    
    // Funcion para comprobar si estas en el suelo correcto
    public void verSuelo() {
    	if (mario.getTamanio() == 0) {
    		suelo_Mario = suelo;
    		caida = true;
    		caidaFun();
    	}
    	if ( mario.getTamanio() == 1 || mario.getTamanio() == 2 ) {
    		// Te da el suelo
    		suelo_Mario = suelo - mario.getAltoMario() / 2;
    		caida = true;
    		caidaFun();
    	}
    }
    
    // Funcion cuando cojes seta
    public void cojeSeta() {
    	for (int i = 0; i < posSeta.length; i++) {
        	if (Math.round(xGame / ANCHO_bloques) == posSeta[i][0] &&
        			suelo_Mario / ALTO_bloques - Math.round(y / ALTO_bloques) == posSeta[i][1]) {
        		if (mario.getTamanio() == 0) {
        			posSeta[i][0] = -10;
        			cambiaTamanio("Grande");
        		}
        		else
        			posSeta[i][0] = -10;
        	}
    	}
    }
    
    // Funcion cuando cojes flor
    public void cojeFlor() {
    	for (int i = 0; i < posFlor.length; i++) {
        	if (Math.round(xGame / ANCHO_bloques) == posFlor[i][0] &&
        			suelo_Mario / ALTO_bloques - Math.round(y / ALTO_bloques) == posFlor[i][1]) {
        		if (mario.getTamanio() == 0 || mario.getTamanio() == 1) {
        			posFlor[i][0] = -10;
        			cambiaTamanio("Fuego");
        		}
        		else
        			posFlor[i][0] = -10;
        	}
    	}
    }
    
    public void cojeMoneda(int num_caja) {
    	if (cajasSorpresa[num_caja].getTipoCaja() == 1) {
    		cajasSorpresa[num_caja].setNombreImagen("cajaVacia");
    		cajasSorpresa[num_caja].setTipoCaja(0);
			coin++;
    	}
    }
    
    // Formas de Mario
    public void formas() {
    	
    	if (derecha && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Derecha1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Derecha2.ordinal();
    		else
    			forma = formas.Derecha3.ordinal();
    	}
    	
    	if (izquierda && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Izquierda1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Izquierda2.ordinal();
    		else
    			forma = formas.Izquierda3.ordinal();
    	}

		animacion1++;
		if(animacion1 % 300 == 0)
			animacion++;
		
    	if ( (!derecha && !izquierda) || (derecha && izquierda) )
    		forma = formas.Quieto.ordinal();
    	
    	if (salto || caida)
    		forma = formas.Salto_Der.ordinal();
    	if (salto && izquierda)
    		forma = formas.Salto_Iz.ordinal();
    }
    
    // Funcion para cambiar tamanio
    public void cambiaTamanio(String size) {
    	
    	if (size == "Peque") {
			mario.setTamanio(0);
    		mario.setAltoMario(ALTO_bloques);
    		mario.setLongitudSalto(mario.getLongitudSalto() - mario.getAltoMario());
    		suelo_Mario = suelo;
			y = suelo_Mario;
    	}
    	
    	if (size == "Grande") {
			mario.setTamanio(1);
    		mario.setAltoMario(ALTO_bloques * 2);
    		mario.setLongitudSalto(mario.getLongitudSalto() - ALTO_bloques);
	        suelo_Mario = suelo - mario.getAltoMario() / 2;
			y = suelo_Mario;
    	}
    	
    	if (size == "Fuego") {
			mario.setTamanio(2);
    		mario.setAltoMario(ALTO_bloques * 2);
    		mario.setLongitudSalto(mario.getLongitudSalto() - ALTO_bloques);
	        suelo_Mario = suelo - mario.getAltoMario() / 2;
			y = suelo_Mario;
    	}
    }
    
    public void lanza_fuego () {
    	
    	fire = true;
    	
    	fuegoPosXini = x + 50;
    	fuegoPosY = y + ALTO_Mario;
    	fuegoPosXReal = xGame + 50;
    	fuegoPosX = fuegoPosXini;
    }
    
    public void reset () {
        x = 0;
        y = suelo_Mario;
        xGame = 0;
        x_fondo = 0;
        ANCHO_fondo = ANCHO;
        
        empieza = false;
        entrar = false;
        end = false;
        
        coin = 0;
        
        iniciar_Bonificaciones ();
        
        for (int i = 0; i < cajasSorpresa.length; i++) {
        	cajasSorpresa[i].setNombreImagen("cajaSorpresa");
    		cajasSorpresa[i].setTipoCaja(1);
        }
    }
    
    public void finalPrograma () throws Exception {
    	
    	vidas --;
    	if (vidas >= 0) {
    		reset();
    		musicaMuerto.play();
    		comienzo.cicloJuego();
    		
    	}
    	else
    		end = true;
    	dibuja();
    }
    
    public void animacionFinal() throws Exception {
    	musicaFinal.play();
    	end = false;
    	int condicion = 0;
    	
    	if (mario.getTamanio() != 0)
    		condicion = ALTO_Mario;
    	
    	while (Math.round(y) != suelo - condicion) {
    		salto = true;
    		y += velocidad / 2;
    		formas ();
    		dibuja ();
    	}
    	    	
    	while (!end) {
        	salto = false;
        	derecha = true;
    		xGame += velocidad / 2;
    		x += velocidad / 2;
    		formas ();
    		dibuja ();
    		// Una posible es +700
    		if (Math.round(xGame) >= (x_final + 700) )
    			end = true;
    	}

    	salir_fin = false;
    	
    	while (!salir_fin) {
    		dibuja ();
    	}
    	musicaFinal.stop();
    	marco.dispose();
	}
    
    public void iniciar_Bonificaciones () {
    	
    	posFlor = new int[][] {
    		{42, 0},
    		{113, 0}
    	};
    	
    	posSeta = new int[][] {
    		{38, 0},
    		{66, 0},
    		{151, 0}
    	};
    }
}